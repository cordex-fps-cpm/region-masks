# Description: Dissolve agglomeration regions of 'Grossregionen der Schweiz'
#              into neighbouring regions
#
# Author: Christian R. Steger, November 2022

# Load modules
import os
import numpy as np
import matplotlib.pyplot as plt
import matplotlib as mpl
import matplotlib.gridspec as gridspec
import fiona
from shapely.geometry import shape, mapping
from shapely.geometry.multipolygon import MultiPolygon
from shapely.geometry import LineString
from shapely.ops import split, unary_union
from descartes import PolygonPatch

mpl.style.use("classic")

# Paths to folders
path_reg = "/project/pr133/csteger/Miscellaneous/CORDEX_FPS_CPM/Shapefiles/"
path_plot = "/users/csteger/Plots/Miscellaneous/CORDEX_FPS_CPM/"
# path_reg = "/Users/csteger/Desktop/"
# path_plot = "/Users/csteger/Desktop/"

###############################################################################
# Overview plot
###############################################################################

# Load polygons
reg_poly = {}
ds = fiona.open(path_reg + "Grossraeume_neu/Grossraeume_neu.shp")
schema = ds.schema
crs = ds.crs
for ind in range(len(ds)):
    reg = ds[ind]["properties"]["Name_Prodr"]
    poly_shp = shape(ds[ind]["geometry"])
    reg_poly[reg] = poly_shp
ds.close()

# Separate agglomeration polygons
poly_aglo = reg_poly["Grosse Agglomerationen"]
print("Number of agglomeration regions: " + str(len(poly_aglo)))
reg_names_keep = list(reg_poly.keys())
reg_names_keep.remove("Grosse Agglomerationen")

# Check input polygons for overlapping regions
plt.figure(figsize=(12, 7))
ax = plt.axes()
for name in reg_names_keep:
    poly_plot = PolygonPatch(reg_poly[name], facecolor="red",
                             edgecolor="black", zorder=1, linewidth=1.0,
                             alpha=0.5)
    ax.add_patch(poly_plot)
poly_plot = PolygonPatch(poly_aglo, facecolor="blue", edgecolor="black",
                         zorder=1, linewidth=1.0, alpha=0.5)
ax.add_patch(poly_plot)
ax.autoscale()
plt.show()

# Find agglomeration polygons with only one neighbour
if all([poly.geom_type == "Polygon" for poly in poly_aglo]):
    print("All agglomeration regions are polygons")
else:
    raise ValueError("Not all agglomerationen regions are polygons")
neighbour = ["multiple"] * 12
for ind, poly in enumerate(poly_aglo):
    poly_ext = poly.buffer(100.0)
    overlap = [reg_poly[name].overlaps(poly_ext) for name in reg_names_keep]
    if sum(overlap) == 1:
        neighbour[ind] = reg_names_keep[overlap.index(True)]

# Define splitting lines for agglomeration polygons (-> from below plot)
split_line = {2: (1363, 184), 11: (151, 269),
              9: (1766, 1976), 8: (165, 127),
              6: (320, 197)}  # initially: (320, 122)

# Overview plot
colors = {"Alpen": "darkkhaki", "Alpensüdseite": "darkgoldenrod",
          "Grosse Agglomerationen": "red", "Jura": "darkgreen",
          "Mittelland": "green", "Voralpen": "gold"}
# -----------------------------------------------------------------------------
plt.figure(figsize=(12, 7))
ax = plt.axes()
for name in reg_names_keep:
    poly_plot = PolygonPatch(reg_poly[name], facecolor=colors[name],
                             edgecolor="black", zorder=1, linewidth=1.0,
                             label=name)
    ax.add_patch(poly_plot)
# -----------------------------------------------------------------------------
for ind, poly in enumerate(poly_aglo):
    poly_plot = PolygonPatch(poly, facecolor="none", edgecolor="black",
                             hatch="///", zorder=2, linewidth=1.0,
                             alpha=0.5)
    ax.add_patch(poly_plot)
    x_cen, y_cen = poly.centroid.xy
    plt.text(x_cen[0], y_cen[0], ind, fontsize=12, fontweight="bold",
             color="red")
    if neighbour[ind] == "multiple":
        x_poly, y_poly = poly.exterior.coords.xy
        plt.scatter(x_poly, y_poly, s=30, color="black", zorder=3)
        for k in range(0, len(x_poly), 10):
            plt.text(x_poly[k], y_poly[k], str(k), fontsize=10, color="black",
                     zorder=3)
        if "split_line" in locals():
            x_mark = np.array(x_poly)[np.array(split_line[ind])]
            y_mark = np.array(y_poly)[np.array(split_line[ind])]
            plt.scatter(x_mark, y_mark, s=100, marker="*", color="blue",
                        zorder=4)
# -----------------------------------------------------------------------------
ax.autoscale()
plt.legend(frameon=False, fontsize=10, loc="lower right")
plt.show()

###############################################################################
# Split agglomeration polygons (if necessary) and assign all polygons to other
# regions
###############################################################################

# Split polygons
ind_split = np.where([name == "multiple" for name in neighbour])[0]
poly_split = {}
for ind in ind_split:
    x_poly, y_poly = poly_aglo[ind].exterior.coords.xy
    x_line = [x_poly[i] for i in split_line[ind]]
    y_line = [y_poly[i] for i in split_line[ind]]
    splitter = LineString(zip(x_line, y_line))
    poly_split[ind] = split(poly_aglo[ind], splitter)
    if len(poly_split[ind]) != 2:
        raise ValueError("Error during splitting")

# Test plots
cols = ("blue", "red")
for ind in ind_split:
    plt.figure()
    ax = plt.axes()
    poly_plot = PolygonPatch(poly_aglo[ind], facecolor="grey",
                             edgecolor="black",
                             zorder=1, linewidth=1.0)
    ax.add_patch(poly_plot)
    for i, poly in enumerate(poly_split[ind]):
        poly_plot = PolygonPatch(poly, facecolor=cols[i], edgecolor="black",
                                 zorder=1, linewidth=2.0)
        ax.add_patch(poly_plot)
        x_cen, y_cen = poly.centroid.xy
        plt.text(x_cen[0], y_cen[0], i, fontsize=12, fontweight="bold",
                 color="black")
    plt.title("Agglomeration " + str(ind), fontsize=12, fontweight="bold")
    ax.autoscale()
    plt.show()

# Manually assign split polygons to regions
poly_split_reg = {
    "Jura": [poly_split[2][0], poly_split[11][0]],
    "Mittelland": [poly_split[2][1], poly_split[6][0], poly_split[8][0],
                   poly_split[9][0], poly_split[11][1]],
    "Voralpen": [poly_split[6][1], poly_split[8][1], poly_split[9][1]]
}

###############################################################################
# Dissolve agglomeration polygons into neighbouring regions
###############################################################################

# Compile all relevant polygons
reg_merge = {name: [] for name in reg_names_keep}
# ---------------------- assign original polygons -----------------------------
for name in list(reg_poly.keys()):
    if name in list(reg_merge.keys()):
        reg_merge[name].append(reg_poly[name])
# ---------- assign agglomeration polygons with only one neighbour ------------
for ind, poly in enumerate(poly_aglo):
    if neighbour[ind] in list(reg_merge.keys()):
        reg_merge[neighbour[ind]].append(poly)
# ------------------- assign split agglomeration polygons ---------------------
for name in list(poly_split_reg.keys()):
    if name in list(reg_merge.keys()):
        reg_merge[name].extend(poly_split_reg[name])
# -----------------------------------------------------------------------------

# Merge polygons
reg_poly_war = {}  # without agglomeration regions
for name in reg_names_keep:
    reg_poly_war[name] = unary_union(reg_merge[name])

# Remove Fricktal from Jura polygon
poly_inters = reg_poly_war["Jura"].intersection(reg_poly_war["Mittelland"])
if any([i.type == "MultiPolygon" for i in poly_inters]):
    raise ValueError("Geometry Collection contains MultiPolygon")
poly_inters = MultiPolygon([i for i in poly_inters if i.type == "Polygon"])
reg_poly_war["Jura"] = reg_poly_war["Jura"] - poly_inters

###############################################################################
# Create final plot and save new regions to shape file
###############################################################################

# Plot for final regions
fig = plt.figure(figsize=(10, 14))
gs = gridspec.GridSpec(2, 1, left=0.1, bottom=0.1, right=0.9,
                       top=0.9, hspace=0.1, wspace=0.04)
# -----------------------------------------------------------------------------
ax = plt.subplot(gs[0])
for name in list(reg_poly.keys()):
    poly_plot = PolygonPatch(reg_poly[name], facecolor=colors[name],
                             edgecolor="black", zorder=1, linewidth=1.0,
                             label=name)
    ax.add_patch(poly_plot)
plt.axis([470000.0, 850000.0, 55000.0, 305000.0])
plt.xticks([])
plt.yticks([])
plt.legend(frameon=False, fontsize=10, loc="lower right")
plt.title("Grossregionen der Schweiz (6)", fontsize=12, fontweight="bold",
          y=1.005)
# -----------------------------------------------------------------------------
ax = plt.subplot(gs[1])
for name in reg_names_keep:
    poly_plot = PolygonPatch(reg_poly_war[name], facecolor=colors[name],
                             edgecolor="black", zorder=1, linewidth=1.0,
                             alpha=0.5)
    ax.add_patch(poly_plot)
plt.axis([470000.0, 850000.0, 55000.0, 305000.0])
plt.xticks([])
plt.yticks([])
plt.title("Grossregionen der Schweiz (5; ohne Agglomerationen)",
          fontsize=12, fontweight="bold", y=1.005)
# -----------------------------------------------------------------------------
fig.savefig(path_plot + "Grossregionen_Schweiz.png", dpi=500,
            bbox_inches="tight")
plt.close(fig)

# Merge all region polygons -> Switzerland polygon
reg_poly_war["Schweiz"] \
    = unary_union([reg_poly_war[i] for i in reg_poly_war.keys()])

# Save new regions to shapefile
del schema["properties"]["Shape_Leng"]
del schema["properties"]["Shape_Area"]
del schema["properties"]["OBJECTID"]
path_out = path_reg + "Grossraeume_neu_without_aglo/"
if not os.path.exists(path_out):
    os.makedirs(path_out)
with fiona.open(path_out + "Grossraeume_neu_without_aglo.shp", "w",
                crs=crs, driver="ESRI Shapefile",
                schema=schema) as output:
    for name in list(reg_poly_war.keys()):
        prop = {"Name_Prodr": name}
        output.write({"geometry": mapping(reg_poly_war[name]),
                      "properties": prop})
