# Description: Plot 'Grossregionen der Schweiz' without agglomerations
#
# Author: Christian R. Steger, March 2023

# Load modules
import matplotlib.pyplot as plt
import matplotlib as mpl
import fiona
from shapely.geometry import shape
from shapely.geometry import MultiPolygon, Polygon
from descartes import PolygonPatch
import cartopy.crs as ccrs
import cartopy.feature as cfeature
from pyproj.crs import CRS
import numpy as np

mpl.style.use("classic")

# Paths to folders
path_reg = "/Users/csteger/Downloads/region-masks-main/Shapefiles/"
path_plot = "/Users/csteger/Downloads/"

###############################################################################
# Plot
###############################################################################

# Load polygons
reg_poly = {}
ds = fiona.open(path_reg + "Grossraeume_neu_without_aglo/"
                + "Grossraeume_neu_without_aglo.shp")
schema = ds.schema
crs = ds.crs
for ind in range(len(ds)):
    reg = ds[ind]["properties"]["Name_Prodr"]
    poly_shp = shape(ds[ind]["geometry"])
    reg_poly[reg] = poly_shp
ds.close()
del reg_poly["Schweiz"]

# Quick fix to remove 'artefacts' in Mittelland polygon
# [i.area for i in reg_poly["Mittelland"]]
reg_poly["Mittelland"] = MultiPolygon([reg_poly["Mittelland"][0],
                                       reg_poly["Mittelland"][1],
                                       reg_poly["Mittelland"][2],
                                       Polygon(reg_poly["Mittelland"][3]
                                               .exterior)])

# Plot regions
colors = {"Alpen": "darkkhaki", "Alpensüdseite": "darkgoldenrod",
          "Jura": "darkgreen", "Mittelland": "green", "Voralpen": "gold"}
map_ext = np.array([5.85, 10.65, 45.7, 47.9])  # [degree]
rad_earth = 6371.0  # approximate radius of Earth [km]
dist_x = 2.0 * np.pi * (rad_earth * np.cos(np.deg2rad(map_ext[2:].mean()))) \
         / 360.0 * (map_ext[1] - map_ext[0])
dist_y = 2.0 * np.pi * rad_earth / 360.0 * (map_ext[3] - map_ext[2])
# -----------------------------------------------------------------------------
fig = plt.figure(figsize=(12.0, 12.0 * (dist_y / dist_x)))
ax = plt.axes(projection=ccrs.PlateCarree())
for name in list(reg_poly.keys()):
    poly_plot = PolygonPatch(reg_poly[name], facecolor=colors[name],
                             edgecolor="black", zorder=2, linewidth=1.0,
                             label=name, transform=ccrs.Projection(crs))
    ax.add_patch(poly_plot)
# -----------------------------------------------------------------------------
# bord_10m = cfeature.NaturalEarthFeature("cultural",
#                                         "admin_0_boundary_lines_land",
#                                         "10m",
#                                         edgecolor="black",
#                                         facecolor="none", zorder=1)
# ax.add_feature(bord_10m)
# -----------------------------------------------------------------------------
gl = ax.gridlines(crs=ccrs.PlateCarree(),
                  xlocs=np.arange(0.0, 90.0, 0.5),
                  ylocs=np.arange(0.0, 90.0, 0.5),
                  linewidth=1, color="None", alpha=1.0, linestyle=":",
                  draw_labels=True)
gl.top_labels = False
gl.right_labels = False
ax.set_aspect("auto")
ax.set_extent(map_ext, crs=ccrs.PlateCarree())
labels = list(reg_poly.keys())  # german
# labels = ["Alps", "Southern Alps", "Jura", "Swiss Plateau",
#           "Alpine foothills"]  # english
# Voralpen: "Alpine foothills", "Pre-Alps"
# Alpensüdseite: "South side of the Alps", "Southern Alps"
plt.legend(frameon=False, fontsize=10.5, loc="lower right", labels=labels)
# -----------------------------------------------------------------------------
fig.savefig(path_plot + "Grossregionen_Schweiz.png", dpi=300,
            bbox_inches="tight")
plt.close(fig)

