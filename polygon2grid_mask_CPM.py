# Description: Create grid masks from polygon subregions of Switzerland
#
# Author: Christian R. Steger, January 2023

# Load modules
import numpy as np
import xarray as xr
import matplotlib.pyplot as plt
import matplotlib as mpl
from pyproj import CRS, Transformer
import cartopy.crs as ccrs
import cartopy.feature as cfeature
import fiona
from shapely.geometry import shape
from shapely.ops import transform
import shapely.vectorized
from descartes import PolygonPatch
import time
import warnings
from shapely.errors import ShapelyDeprecationWarning

mpl.style.use("classic")

# Paths to folders
path_model = "/store/c2sm/c2sme/CH202X/CORDEX-FPSCONV/ALP-3/"
path_obs = "/store/c2sm/c2sme/CH202X/OBS/day/MCH/"
path_reg = "/project/pr133/csteger/Miscellaneous/CORDEX_FPS_CPM/Shapefiles/"
path_plot = "/users/csteger/Plots/Miscellaneous/CORDEX_FPS_CPM/"
path_out = "/project/pr133/csteger/Temp/"

###############################################################################
# Load and check regions of Switzerland
###############################################################################

# Skip shapely deprecation warning
warnings.filterwarnings("ignore", category=ShapelyDeprecationWarning)

# Load 6 polygons (5 sub-regions and entire Switzerland)
poly_reg = {}
file = "Grossraeume_neu_without_aglo.shp"
ds = fiona.open(path_reg + "Grossraeume_neu_without_aglo/" + file)
for i in ds:
    name = i["properties"]["Name_Prodr"]
    poly_reg[name] = shape(i["geometry"])
crs_reg = CRS.from_string(ds.crs["init"])
ds.close()
# -> polygons inspected with QGIS -> contain negligible 'imperfections'
#    on a very small scale...

# # Test plot
# name = "Schweiz"
# plt.figure()
# ax = plt.axes()
# poly_plot = PolygonPatch(poly_reg[name], facecolor="grey", edgecolor="black",
#                          zorder=1, linewidth=3.0, alpha=0.5)
# ax.add_patch(poly_plot)
# ax.autoscale()
# plt.show()

# Check type of geometries and number of (multi-)polygons
poly_CH = poly_reg["Schweiz"]
del poly_reg["Schweiz"]  # discard Switzerland polygon
num_poly = 0
for name in poly_reg.keys():
    if poly_reg[name].geom_type == "Polygon":
        num_poly += 1
    elif poly_reg[name].geom_type == "MultiPolygon":
        num_poly += len(poly_reg[name])
    else:
        raise ValueError("Unexpected geometry type found")
print("Number of regions: " + str(len(poly_reg)))
print("Number of polygons: " + str(num_poly))

# Test plot
plt.figure()
ax = plt.axes(projection=ccrs.PlateCarree())
for name in list(poly_reg.keys()):
    poly_plot = PolygonPatch(poly_reg[name], facecolor="grey", edgecolor="red",
                             alpha=0.5, zorder=2, linewidth=1.0,
                             transform=ccrs.Projection(crs_reg))
    ax.add_patch(poly_plot)
gl = ax.gridlines(crs=ccrs.PlateCarree(), draw_labels=True,
                  linewidth=2, color="black", alpha=0.5, linestyle="-")
gl.top_labels = False
gl.right_labels = False
ax.autoscale()
plt.show()

###############################################################################
# Compute binary and fractional grid masks for CPMs and observation(s)
###############################################################################

# Files with grid information of products
products = {
    # -------------------------------------------------------------------------
    "CLMcom-BTU-CCLM5-0-14":
        path_model + "fx/orog/orog_ALP-3_ECMWF-ERAINT_evaluation_"
        + "r0i0p0_CLMcom-BTU-CCLM5-0-14_fpsconv-x2yn2-v1_fx.nc",
    "CLMcom-KIT-CCLM5-0-14":
        path_model + "fx/orog/orog_ALP-3_ECMWF-ERAINT_evaluation_"
        + "r0i0p0_CLMcom-KIT-CCLM5-0-14_fpsconv-x2yn2-v1_fx.nc",
    "GERICS-REMO2015":
        path_model + "fx/orog/orog_ALP-3_ECMWF-ERAINT_evaluation_"
        + "r0i0p0_GERICS-REMO2015_v1_fx.nc",
    "HCLIMcom-HCLIM38-AROME":
        path_model + "fx/orog/orog_ALP-3_ECMWF-ERAINT_evaluation_"
        + "r0i0p0_HCLIMcom-HCLIM38-AROME_fpsconv-x2yn2-v1_fx.nc",
    "ICTP-RegCM4-7":
        path_model + "fx/orog/orog_ALP-3_ECMWF-ERAINT_evaluation_"
        + "r0i0p0_ICTP-RegCM4-7_fpsconv-x2yn2-v1_fx.nc",
    "BCCR-WRF381BF":
        path_model + "fx/orog/orog_ALP-3_ECMWF-ERAINT_evaluation_"
        + "r1i1p1_BCCR-WRF381BF_v01_fx.nc",
    "COSMO-pompa_5.0_2019.1":
        path_model + "fx/orog/orog_ALP-3_ECMWF-ERAINT_evaluation_"
        + "r1i1p1_COSMO-pompa_5.0_2019.1_fx.nc",
    "HCLIM38h1-AROME":
        path_model + "fx/orog/orog_ALP-3_ECMWF-ERAINT_evaluation_"
        + "r1i1p1_HCLIM38h1-AROME_fpsconv-x2yn2-v1_fx.nc",
    "IPSL-WRF381CE":
        path_model + "fx/orog/orog_ALP-3_IPSL-CM5A-MR_historical_"
        + "r1i1p1_IPSL-WRF381CE_fpsconv-x2yn2-v1_fx.nc",
    "CLMcom-JLU-CCLM5-0-15":
        path_model + "fx/orog/orog_ALP-3_MPI-M-MPI-ESM-LR_historical_"
        + "r0i0p0_CLMcom-JLU-CCLM5-0-15_fpsconv-x0n1-v1_fx.nc",
    "CLMcom-KIT-CCLM5-0-15":
        path_model + "fx/orog/orog_ALP-3_MPI-M-MPI-ESM-LR_historical_"
        + "r0i0p0_CLMcom-KIT-CCLM5-0-15_fpsconv-x2yn2-v1_fx.nc",
    # -------------------------------------------------------------------------
    "UCAN-WRF381BI":
        path_model + "fx/sftls/sftls_ALP-3_ECMWF-ERAINT_evaluation_"
        + "r1i1p1_UCAN-WRF381BI_x1n2_fx.nc",
    "IDL-WRF381BH":
        path_model + "fx/sftlf/sftlf_ALP-3_ECMWF-ERAINT_evaluation_"
        + "r1i1p1_IDL-WRF381BH_fpsconv-x1n2-v1_fx.nc",
    # -------------------------------------------------------------------------
    "AUTH-MC-WRF381BG":
        path_model + "day/tas/evaluation/tas_ALP-3_ECMWF-ERAINT_evaluation_"
        + "r1i1p1_AUTH-MC-WRF381BG_v01_day_20000101-20001231.nc",
    "HadREM3-RA-UM10.1":
        path_model + "day/tas/evaluation/tas_ALP-3_ECMWF-ERAINT_evaluation_"
        + "r1i1p1_HadREM3-RA-UM10.1_fpsconv-x0n1-v1_day_20000101-20000131.nc",
    "WEGC-WRF381BL":
        path_model + "day/tas/evaluation/tas_ALP-3_ECMWF-ERAINT_evaluation_"
        + "r1i1p1_WEGC-WRF381BL_x2yn2v1_day_20000101-20000131.nc",
    "FZJ-IBG3-WRF381BB":
        path_model + "day/tas/evaluation/tas_ALP-3_ECMWF-ERAINT_evaluation_"
        + "r1i1p1_FZJ-IBG3-WRF381BB_v01_day_20000101-20001231.nc",
    "CICERO-WRF381BJ":
        path_model + "day/tas/evaluation/tas_ALP-3_ECMWF-ERAINT_evaluation_"
        + "r1i1p1_CICERO-WRF381BJ_v01_day_20000101-20001231.nc",
    "UHOH-WRF381BD":
        path_model + "day/tas/evaluation/tas_ALP-3_ECMWF-ERAINT_evaluation_"
        + "r1i1p1_UHOH-WRF381BD_x1n2v1_day_19990101-19991231.nc",
    "CLMcom-DWD-CCLM5-0-15":
        path_model + "day/tas/historical/tas_ALP-3_MOHC-HadGEM2-ES_"
        + "historical_r1i1p1_CLMcom-DWD-CCLM5-0-15_fpsconv-x2yn2-v1_"
        + "day_20000101-20001230.nc",
    "CLMcom-CMCC-CCLM5-0-9":
        path_model + "day/tas/evaluation/tas_ALP-3_ECMWF-ERAINT_evaluation_"
        + "r1i1p1_CLMcom-CMCC-CCLM5-0-9_x2yn2v1_day_20000101-20001231.nc",
    "CLMCom-WEGC-CCLM5-0-09":
        path_model + "day/tas/historical/tas_ALP-3_MPI-M-MPI-ESM-LR_"
        + "historical_r2i1p1_CLMCom-WEGC-CCLM5-0-09_x2yn2v1_day_"
        + "20000701-20000731.nc",
    # "CNRM-AROME41t1":
    #     path_model + "day/tas/evaluation/tas_ALP-3_ECMWF-ERAINT_"
    #     + "evaluation_r1i1p1_CNRM-AROME41t1_fpsconv-x2yn2-v1_day_"
    #     + "20090101-20091231.nc",
    "CNRM-AROME41t1":
        "/project/pr133/csteger/Miscellaneous/CORDEX_FPS_CPM/"
        + "Modified_grid_files/tas_ALP-3_ECMWF-ERAINT_"
        + "evaluation_r1i1p1_CNRM-AROME41t1_fpsconv-x2yn2-v1_day_"
        + "20090101-20091231_setgrid_xy.nc",
    # -------------------------------------------------------------------------
    "OBS_MCH":
        path_obs + "topo.swiss02_ch02.lonlat.nc"
    # -------------------------------------------------------------------------
}

# Loop through different products
dev_thresh = 5e-05  # threshold for printing deviation in coordinates [degree]
for i in list(products.keys()):

    print((" Process product " + i + " ").center(60, "-"))

    # Load grid information
    ds = xr.open_dataset(products[i], decode_times=False)
    # -------------------------------------------------------------------------
    # Products using rotated coordinates
    # -------------------------------------------------------------------------
    if ("rlon" in list(ds.coords)) and ("rlat" in list(ds.coords)):
        print("Product uses rotated coordinates")
        x = ds["rlon"].values
        y = ds["rlat"].values
        if i != "CLMCom-WEGC-CCLM5-0-09":
            keys = {"rotated_latitude_longitude", "rotated_pole",
                    "Rotated_Pole"}
            key = set(ds.variables).intersection(keys).pop()
            pole_latitude = float(ds[key].grid_north_pole_latitude)
            pole_longitude = float(ds[key].grid_north_pole_longitude)
            central_rotated_longitude = 0.0
            if "north_pole_grid_longitude" in list(ds[key].attrs):
                central_rotated_longitude \
                    = float(ds[key].north_pole_grid_longitude)
        # --------------------------------------------------------------------m
        else:
            print("Use hard-coded rotated pole information")
            pole_latitude = 39.25
            pole_longitude = -162.0
            central_rotated_longitude = 0.0
        # --------------------------------------------------------------------m
        mod_cor = ("BCCR-WRF381BF", "IPSL-WRF381CE", "UCAN-WRF381BI",
                   "WEGC-WRF381BL", "FZJ-IBG3-WRF381BB", "CICERO-WRF381BJ",
                   "UHOH-WRF381BD")
        if i in mod_cor:
            print("Manually correct rotated pole information")
            pole_longitude -= 180.0
        # --------------------------------------------------------------------m
        if i == "AUTH-MC-WRF381BG":
            print("Overwrite rotated grid/pole information with BCCR-WRF381BF")
            ds_ref = xr.open_dataset(products["BCCR-WRF381BF"])
            x = ds_ref["rlon"].values
            y = ds_ref["rlat"].values
            key = "rotated_pole"
            pole_latitude = float(ds_ref[key].grid_north_pole_latitude)
            pole_longitude \
                = float(ds_ref[key].grid_north_pole_longitude) - 180.0
            ds_ref.close()
        # ---------------------------------------------------------------------
        crs_rot = ccrs.RotatedPole(
            pole_latitude=pole_latitude, pole_longitude=pole_longitude,
            central_rotated_longitude=central_rotated_longitude)
        proj_prod = CRS.from_proj4(crs_rot.proj4_init)
    # ------------------------------------------------------------------------m
    elif i == "CLMcom-CMCC-CCLM5-0-9":
        print("Product uses rotated coordinates")
        print("Get rotated coordinates from CLMcom-KIT-CCLM5-0-14")
        ds_ref = xr.open_dataset(products["CLMcom-KIT-CCLM5-0-14"])
        x = ds_ref["rlon"].values
        y = ds_ref["rlat"].values
        ds_ref.close()
        key = "rotated_pole"
        pole_latitude = float(ds[key].grid_north_pole_latitude)
        pole_longitude = float(ds[key].grid_north_pole_longitude)
        central_rotated_longitude = 0.0
        crs_rot = ccrs.RotatedPole(
            pole_latitude=pole_latitude, pole_longitude=pole_longitude,
            central_rotated_longitude=central_rotated_longitude)
        proj_prod = CRS.from_proj4(crs_rot.proj4_init)
    # -------------------------------------------------------------------------
    # Products using map projections
    # -------------------------------------------------------------------------
    elif ("x" in list(ds.coords)) and ("y" in list(ds.coords)):
        print("Product uses map projection")
        x = ds["x"].values
        y = ds["y"].values
        if i != "CNRM-AROME41t1":
            keys = {"crs", "Lambert_Conformal"}
            key = set(ds.variables).intersection(keys).pop()
            if "proj4_params" in list(ds[key].attrs):
                proj_prod_str = ds[key].proj4_params
            elif "proj4" in list(ds[key].attrs):
                proj_prod_str = ds[key].proj4
            else:
                raise ValueError("Unknown attribute for projection")
            # ----------------------------------------------------------------m
            if i == "ICTP-RegCM4-7":
                print("Manually correct map projection information")
                proj_prod_str = proj_prod_str.replace("+x_0=-1500.",
                                                      "+x_0=0.") \
                    .replace("+y_0=-1500.", "+y_0=0.")
                proj_prod_str = proj_prod_str \
                    .replace("+lat_0=45.44", "+lat_0=45.441") \
                    .replace("+lon_0=8.06", "+lon_0=8.062")
            elif i == "HCLIMcom-HCLIM38-AROME":
                print("Manually correct map projection information")
                proj_prod_str = proj_prod_str.replace("+proj=lcca",
                                                      "+proj=lcc")
            # -----------------------------------------------------------------
            proj_prod = CRS.from_proj4(proj_prod_str)
        # --------------------------------------------------------------------m
        else:
            print("Use hard-coded map projection information")
            proj_prod_dict = {"proj": "lcc", "lat_1": 44.88, "lat_2": 44.88,
                              "lat_0": 44.88, "lon_0": 8.48, "k_0": 1.0,
                              "x_0": 732500.0, "y_0": 607500.0,
                              "a": 6371220.0, "b": 6371220.0}
            x *= 1000.0  # [km] -> [m]
            y *= 1000.0  # [km] -> [m]
            proj_prod = CRS.from_dict(proj_prod_dict)
    # -------------------------------------------------------------------------
    # Products using geographic coordinates
    # -------------------------------------------------------------------------
    elif ("lon" in list(ds.coords)) and ("lat" in list(ds.coords)):
        print("Product uses geographic coordinates")
        x = ds["lon"].values
        y = ds["lat"].values
        proj_prod = CRS.from_string("epsg:4326")
    # -------------------------------------------------------------------------
    else:
        raise ValueError("Product has unknown grid")
    # -------------------------------------------------------------------------
    if ("lon" in list(ds.coords)) and ("lat" in list(ds.coords)):
        lon = ds["lon"].values
        lat = ds["lat"].values
    elif ("longitude" in list(ds.coords)) and ("latitude" in list(ds.coords)):
        lon = ds["longitude"].values
        lat = ds["latitude"].values
    else:
        raise ValueError("No geographic coordinates found")
    ds.close()

    # Transform coordinates of (multi-)polygons
    project = Transformer.from_crs(crs_reg, proj_prod,
                                   always_xy=True).transform
    poly_reg_trans = {}
    for j in list(poly_reg.keys()):
        poly_reg_trans[j] = transform(project, poly_reg[j])

    # Compute fractional mask (sample multiple times per grid cell)
    num_ss = 10  # subsampling number (in one direction; 10 -> 10 * 10)
    t_beg = time.time()
    d_x_h = np.diff(x).mean() / 2.0
    d_y_h = np.diff(y).mean() / 2.0
    x_samp_grid = np.linspace(x[0] - d_x_h, x[-1] + d_x_h, len(x) * num_ss + 1)
    y_samp_grid = np.linspace(y[0] - d_y_h, y[-1] + d_y_h, len(y) * num_ss + 1)
    x_samp = x_samp_grid[:-1] + np.diff(x_samp_grid) / 2.0
    y_samp = y_samp_grid[:-1] + np.diff(y_samp_grid) / 2.0
    masks_frac = np.empty((len(poly_reg_trans), len(y), len(x)),
                          dtype=np.float32)
    reg_names = list(poly_reg_trans.keys())
    for j, name in enumerate(reg_names):

        # Sample grid cells (multiple times)
        mask_frac_ss = shapely.vectorized.contains(
            poly_reg_trans[name], *np.meshgrid(x_samp, y_samp)) \
            .astype(np.float32)
        # -> in case a grid cell centre coincides exactly with a polygon edge,
        #    the grid cell is not assigned to any polygon. This is avoided
        #    when subsampling is applied.

        # Aggregate samples back to original grid
        y_agg = np.arange(0, mask_frac_ss.shape[0], num_ss)
        temp = np.add.reduceat(mask_frac_ss, y_agg, axis=0)
        x_agg = np.arange(0, mask_frac_ss.shape[1], num_ss)
        masks_frac[j, :, :] = np.add.reduceat(temp, x_agg, axis=1) \
            / (num_ss ** 2)

    grid_masks = np.argmax(masks_frac, axis=0).astype(np.int32)
    area_frac_tot = masks_frac.sum(axis=0)
    grid_masks[area_frac_tot < 0.5] = -1
    print("Computation of fractional masks: " + "%.2f" % (time.time() - t_beg)
          + " s")

    # Colormap
    colors = {"Alpen": "darkkhaki", "Alpensüdseite": "darkgoldenrod",
              "Jura": "darkgreen", "Mittelland": "green", "Voralpen": "gold"}
    cmap = mpl.colors.ListedColormap([colors[i] for i in reg_names])
    bounds = [-0.5, 0.5, 1.5, 2.5, 3.5, 4.5]
    norm = mpl.colors.BoundaryNorm(bounds, cmap.N)

    # Plot masks
    fig = plt.figure(figsize=(12.0, 8.0))
    # -------------------------------------------------------------------------
    ax = plt.axes(projection=ccrs.PlateCarree())
    data_plot = np.ma.masked_where(grid_masks == -1, grid_masks)
    plt.pcolormesh(lon, lat, data_plot, shading="auto",
                   transform=ccrs.PlateCarree(), cmap=cmap, norm=norm)
    land_10m = cfeature.NaturalEarthFeature("physical", "land", "10m",
                                            edgecolor="black",
                                            facecolor="none")
    ax.add_feature(land_10m)
    bord_10m = cfeature.NaturalEarthFeature("cultural",
                                            "admin_0_boundary_lines_land",
                                            "10m",
                                            edgecolor="black",
                                            facecolor="none")
    ax.add_feature(bord_10m)
    poly_plot = PolygonPatch(poly_CH, facecolor="none",
                             edgecolor="red", zorder=1, linewidth=1.0,
                             alpha=1.0, transform=ccrs.Projection(crs_reg))
    ax.add_patch(poly_plot)
    gl = ax.gridlines(crs=ccrs.PlateCarree(), linewidth=1, color="black",
                      alpha=0.5, linestyle=":", draw_labels=True)
    gl.top_labels = False
    gl.right_labels = False
    ax.set_aspect("auto")
    ax.set_extent([5.75, 10.85, 45.55, 47.95], crs=ccrs.PlateCarree())
    # -------------------------------------------------------------------------
    fig.savefig(path_plot + i + "_grid_mask_CH.png", dpi=300,
                bbox_inches="tight")
    plt.close(fig)

    # Check deviations in recomputed geographic coordinates
    transformer = Transformer.from_crs(proj_prod, "EPSG:4326", always_xy=True)
    lon_rc, lat_rc = transformer.transform(*np.meshgrid(x, y))
    lon[lon > 180.0] -= 360.0  # necessary because of GERICS-REMO2015
    if (lon.ndim == 2) and (lat.ndim == 2):
        lon_dev_max = np.abs(lon_rc - lon).max()
        lat_dev_max = np.abs(lat_rc - lat).max()
    else:
        lon_dev_max = np.abs(lon_rc - lon[np.newaxis, :]).max()
        lat_dev_max = np.abs(lat_rc - lat[:, np.newaxis]).max()
    if (lon_dev_max > dev_thresh) or (lat_dev_max > dev_thresh):
        print("Maximal absolute deviation in recomputed geog. coordinates:")
        print("Longitude: %.8f" % np.abs(lon_rc - lon).max())
        print("Latitude: %.8f" % np.abs(lat_rc - lat).max())

    # Save binary mask to NetCDF file (-> save consistent meta data)
    var_rm = ("orog", "sftls", "tas", "height")
    coord_rm = ("time",)
    ds = xr.open_dataset(products[i], decode_times=False)
    ds = ds.drop(list(set(list(ds.variables)) & set(var_rm)))
    if "time" in list(ds.coords):
        ds = ds.drop_dims("time")
    if ("rlat" in list(ds.coords)) and ("rlon" in list(ds.coords)):
        ds["reg_mask"] = (("rlat", "rlon"), grid_masks)
    elif ("y" in list(ds.coords)) and ("x" in list(ds.coords)):
        ds["reg_mask"] = (("y", "x"), grid_masks)
    elif ("lat" in list(ds.coords)) and ("lon" in list(ds.coords)):
        ds["reg_mask"] = (("lat", "lon"), grid_masks)
    else:
        raise ValueError("Unknown spatial coordinates")
    ds.to_netcdf(path_out + i + "_grid_mask_CH.nc")
    ds.close()

# -----------------------------------------------------------------------------
# Explanations for deviations in geographical coordinates
# -----------------------------------------------------------------------------

# ------------- Process product GERICS-REMO2015 --------------
# Maximal absolute deviation in recomputed geog. coordinates:
# Longitude: 0.00006839
# Latitude: 0.00020223
# -> likely reason for difference: rlat/rlon are doubles, lat/lon are floats
# ---------- Process product CLMcom-JLU-CCLM5-0-15 -----------
# Maximal absolute deviation in recomputed geog. coordinates:
# Longitude: 0.00015599
# Latitude: 0.00035614
# -> should be like CLMcom-KIT-CCLM5-0-14 but rlat increasingly differs
#    towards end
# -------------- Process product CNRM-AROME41t1 --------------
# Maximal absolute deviation in recomputed geog. coordinates:
# Longitude: 0.00006595
# Latitude: 0.00005898
# -> minor inaccuracies in provided geographic coordinates

###############################################################################
# Check grid information of individual products
###############################################################################

# -----------------------------------------------------------------------------
# ICTP-RegCM4-7
# -----------------------------------------------------------------------------

# # Load grid information
# ds = xr.open_dataset(products["ICTP-RegCM4-7"])
# x = ds["x"].values
# y = ds["y"].values
# lon = ds["lon"].values
# lat = ds["lat"].values
# proj_prod_str = ds["crs"].proj4_params
# proj_prod_str = proj_prod_str.replace("+x_0=-1500.", "+x_0=0.") \
#     .replace("+y_0=-1500.", "+y_0=0.")
# proj_prod_str = proj_prod_str.replace("+lat_0=45.44", "+lat_0=45.441") \
#     .replace("+lon_0=8.06", "+lon_0=8.062")
# proj_prod = CRS.from_proj4(proj_prod_str)
# ds.close()
#
# # Transform coordinates
# transformer = Transformer.from_crs(proj_prod, "EPSG:4326", always_xy=True)
# lon_rc, lat_rc = transformer.transform(*np.meshgrid(x, y))
#
# # Check mean/maximal absolute deviation
# print("Deviations [degree]:")
# dev_abs = np.abs(lon_rc - lon)
# print("Longitude (mean, max): %.4f" % dev_abs.mean()
#       + ", %.4f" % dev_abs.max())
# dev_abs = np.abs(lat_rc - lat)
# print("Latitude  (mean, max): %.4f" % dev_abs.mean()
#       + ", %.4f" % dev_abs.max())

# -----------------------------------------------------------------------------
# AUTH-MC-WRF381BG
# -----------------------------------------------------------------------------

# # Load grid information
# ds = xr.open_dataset(products["AUTH-MC-WRF381BG"])
# print(ds["Rotated_Pole"])
# lon = ds["lon"].values
# lat = ds["lat"].values
# ds.close()
#
# # Load grid information
# ds = xr.open_dataset(products["BCCR-WRF381BF"])
# print(ds["rotated_pole"])
# lon_cp = ds["lon"].values
# lat_cp = ds["lat"].values
# ds.close()
#
# # Compare geographic coordinates
# print(np.all(lon == lon_cp))
# print(np.all(lat == lat_cp))

# -----------------------------------------------------------------------------
# CLMcom-CMCC-CCLM5-0-9
# -----------------------------------------------------------------------------

# # Load grid information
# ds = xr.open_dataset(products["CLMcom-CMCC-CCLM5-0-9"])
# print(ds["rotated_pole"])
# lon = ds["lon"].values
# lat = ds["lat"].values
# ds.close()
#
# # Load grid information
# ds = xr.open_dataset(products["CLMcom-KIT-CCLM5-0-14"])
# print(ds["rotated_pole"])
# lon_cp = ds["lon"].values
# lat_cp = ds["lat"].values
# ds.close()
#
# # Compare geographic coordinates
# print(np.all(lon == lon_cp))
# print(np.all(lat == lat_cp))

# -----------------------------------------------------------------------------
# CLMCom-WEGC-CCLM5-0-09
# -----------------------------------------------------------------------------

# # Load grid information
# ds = xr.open_dataset(products["CLMCom-WEGC-CCLM5-0-09"])
# rlon = ds["rlon"].values
# rlat = ds["rlat"].values
# lon = ds["lon"].values
# lat = ds["lat"].values
# ds.close()
#
# # Transform coordinates
# crs_rot = ccrs.RotatedPole(pole_latitude=39.25, pole_longitude=-162.0,
#                            central_rotated_longitude=0.0)
# proj_prod = CRS.from_proj4(crs_rot.proj4_init)
# transformer = Transformer.from_crs(proj_prod, "EPSG:4326", always_xy=True)
# lon_rc, lat_rc = transformer.transform(*np.meshgrid(rlon, rlat))
#
# # Check maximal absolute deviation
# print("Deviations [degree]:")
# print("Longitude (max): %.8f" % np.abs(lon_rc - lon).max())
# print("Latitude  (mean, max): %.8f" % np.abs(lat_rc - lat).max())

# -----------------------------------------------------------------------------
# CNRM-AROME41t1
# -----------------------------------------------------------------------------

# # Load grid information
# ds = xr.open_dataset(products["CNRM-AROME41t1"])
# x = ds["x"].values * 1000.0  # [m]
# y = ds["y"].values * 1000.0  # [m]
# lon = ds["lon"].values
# lat = ds["lat"].values
# ds.close()
#
# # Transform coordinates
# proj_prod_dict = {"proj": "lcc", "lat_1": 44.88, "lat_2": 44.88,
#                   "lat_0": 44.88, "lon_0": 8.48, "k_0": 1.0,
#                   "x_0": 732500.0, "y_0": 607500.0,
#                   "a": 6371220.0, "b": 6371220.0}
# # "lat_2": 44.88 is optional; not specifying it does not change anything...
# proj_prod = CRS.from_dict(proj_prod_dict)
# transformer = Transformer.from_crs(proj_prod, "EPSG:4326", always_xy=True)
# lon_rc, lat_rc = transformer.transform(*np.meshgrid(x, y))
#
# # Check mean/maximal absolute deviation
# print("Deviations [degree]:")
# dev_abs = np.abs(lon_rc - lon)
# print("Longitude (mean, max): %.8f" % dev_abs.mean()
#       + ", %.8f" % dev_abs.max())
# dev_abs = np.abs(lat_rc - lat)
# print("Latitude  (mean, max): %.8f" % dev_abs.mean()
#       + ", %.8f" % dev_abs.max())
#
# # Test plot
# plt.figure()
# # plt.pcolormesh(lat_rc - lat)
# plt.pcolormesh(lon_rc - lon)
# plt.colorbar()
# plt.show()
